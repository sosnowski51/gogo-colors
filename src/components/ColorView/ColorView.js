import React from 'react';

import './styles/ColorView.css';
import PropTypes from 'prop-types';

const ColorView = (props) => {
    const color = (props.choosenColor) ? props.choosenColor.hex : 'transparent'
    return (
        <div className="bg-color" style={{ backgroundColor: color }}></div>
    )
}

ColorView.propTypes = {
    choosenColor: PropTypes.object
}

export default ColorView;