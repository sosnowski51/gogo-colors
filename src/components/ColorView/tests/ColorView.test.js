import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import ColorView from '../ColorView';
 
const data = { hex: '#fff' };

it('renders ColorView component without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<ColorView choosenColor={data} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

it('Create colorView component snapshot', () => {
  const component = renderer.create(
    <ColorView choosenColor={data} />,
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
