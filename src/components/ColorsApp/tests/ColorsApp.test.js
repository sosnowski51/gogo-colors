import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import ColorsApp from '../ColorsApp';
 
const data = [
  { name: 'test1', id: 0, hex: '#fff' },
  { name: 'test2', id: 1, hex: '#000' }
]

it('renders ColorsApp component without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ColorsApp colors={data} />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('Create ColorsApp component snapshot', () => {
  const component = renderer.create(
    <ColorsApp colors={data} />,
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
