import React, { Component } from 'react';

import './styles/ColorsApp.css';
import PropTypes from 'prop-types';

import LoaderHOC from '../../HOC/LoaderHOC';
import ColorView from '../ColorView/ColorView';
import SearchBar from '../SearchBar/SearchBar';

class ColorsApp extends Component {
    state = { colors: this.props.colors }

    choosenColor = (choosenColor) => {
        return this.setState({
            choosenColor
        })
    }

    render() {
        const { colors } = this.props;
        return (
            <div className="ColorsApp">
                <ColorView
                    choosenColor={this.state.choosenColor}
                />
                <SearchBar
                    colors={colors}
                    choosenColor={this.choosenColor}
                    startSearchFrom={2}
                />
            </div>
        );
    }

    static propTypes = {
        colors: PropTypes.arrayOf(
            PropTypes.shape({
                id: PropTypes.number.isRequired,
                name: PropTypes.string.isRequired,
                hex: PropTypes.string.isRequired
            }).isRequired,
        )
    }
}

export default LoaderHOC(ColorsApp);