import React, { Component } from 'react';

import './styles/SearchBar.css';
import PropTypes from 'prop-types';

import ColorsProposal from '../ColorsProposal/ColorsProposal';
import acceptedColor from '../../helpers/acceptColor';
import searchColors from '../../helpers/searchColors';

class SearchBar extends Component {
    state = {
        filteredColors: [],
        value: '',
    }

    handleColorSubmit = (inputColor, filteredColors) => (e) => {
        e.preventDefault();
        const color = acceptedColor(inputColor, filteredColors)
        this.setState({
            showList: false
        })
        return this.props.choosenColor(color);
    }

    putColorFromListToInput = (color) => {
        return this.setState({
            filteredColors: [color],
            showList: false,
            value: color.name,
        })
    }

    handleSerachColor = (colorsList) => (e) => {
        const inputValue = e.target.value.toLowerCase();
        const inputChars = e.target.value.length;
        const searchedColors = searchColors(inputChars, inputValue, colorsList, this.props.startSearchFrom);
        return this.setState({ value: inputValue, ...searchedColors });
    }

    render() {
        const { filteredColors, showList, value } = this.state;
        const { colors } = this.props;

        return (
            <div className="search-bar">
                <form className="search-bar__form" onSubmit={this.handleColorSubmit(value, filteredColors)}>
                    <input
                        className="search-bar__input"
                        type="text"
                        placeholder="Color name"
                        value={value}
                        onChange={this.handleSerachColor(colors)}
                    />
                    <button type="submit" className="search-bar__btn">Accept</button>
                </form>
                <ColorsProposal
                    filteredColors={filteredColors}
                    showList={showList}
                    choosenColor={this.putColorFromListToInput}
                />
            </div>
        )
    }

    static propTypes = {
        choosenColor: PropTypes.func.isRequired,
        startSearchFrom: PropTypes.number.isRequired,
        colors: PropTypes.arrayOf(
            PropTypes.shape({
                id: PropTypes.number.isRequired,
                name: PropTypes.string.isRequired,
                hex: PropTypes.string.isRequired
            }).isRequired,
        )
    }
}

export default SearchBar;