import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import SearchBar from '../SearchBar';
 
const colors = [
  { name: 'test1', id: 0, hex: '#fff' },
  { name: 'test2', id: 1, hex: '#000' }
]
const choosenColor = () => { hex: '#fff' };

it('renders SearchBar component without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<SearchBar colors={colors} choosenColor={choosenColor} startSearchFrom={2} />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('Create SearchBar component snapshot', () => {
  const component = renderer.create(
    <SearchBar colors={colors} choosenColor={choosenColor} startSearchFrom={2} />,
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
