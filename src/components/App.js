import React, { Component } from 'react';

import './styles/App.css';

import ColorsApp from './ColorsApp/ColorsApp';
import getData from '../helpers/getData';

class App extends Component {
	state = { colors: [], fetchError: false }

	componentDidMount() {
		getData('http://www.mocky.io/v2/5a37a7403200000f10eb6a2d')
			.then(response => {
				this.setState({
					fetchError: response.fetchError,
					colors: response.colors
				})
			})
	}

	render() {
		const fetchError = this.state.fetchError;

		if (fetchError) {
			return (
				<div className="App">
					<div className='app-render-error'>
						Sorry, but something goes wrong. Can't load data from endpoint.<br />Plese try later or refresh the page
					</div>
				</div>
			)
		}

		return (
			<div className="App">
				<ColorsApp
					colors={this.state.colors}
				/>
			</div>
		);
	}
}

export default App;
