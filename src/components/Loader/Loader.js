import React from 'react';

const Loader = (props) => {
    const extraClass = (props.extraClass) ? props.extraClass : '';
    return (
        <div className={`loader__container ${extraClass}`}>
            <div className="loader_background">
                <div className='loader'>Loading...</div>
            </div>
        </div>
    )
}

export default Loader;