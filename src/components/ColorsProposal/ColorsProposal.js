import React from 'react';

import './styles/ColorsProposal.css';
import PropTypes from 'prop-types';

const ColorsPropsal = (props) => {
    const { filteredColors, showList, choosenColor } = props

    return (
        <div className="colors-proposals">
            <ul className={`colors-proposals__list ${!showList && 'hidden'}`}>
                {showList && filteredColors.map((color) =>
                    <li
                        className="colors-proposals__color"
                        key={color.id}
                        onClick={() => { choosenColor(color) }}
                    >
                        {color.name}
                    </li>
                )}
            </ul>
            
            {!filteredColors.length && 
                <p className="colors-proposals__info">
                    Write more than 2 chars to<br />see proposal colors.
                </p>
            }
        </div>
    )
}

ColorsPropsal.propTypes = {
    choosenColor: PropTypes.func,
    filteredColors: PropTypes.array.isRequired,
    showList: PropTypes.bool
}

export default ColorsPropsal;