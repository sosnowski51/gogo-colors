import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import ColorsProposal from '../ColorsProposal';
 
const colors = [
  { name: 'test1', id: 0, hex: '#fff' },
  { name: 'test2', id: 1, hex: '#000' }
]
const choosenColor = () => { hex: '#fff' };

it('renders ColorsProposal component without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ColorsProposal filteredColors={colors} choosenColor={choosenColor} showList={true} />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('Create ColorsProposal component snapshot', () => {
  const component = renderer.create(
    <ColorsProposal filteredColors={colors} choosenColor={choosenColor} showList={true} />,
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
