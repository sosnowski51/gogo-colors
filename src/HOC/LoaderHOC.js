import React, { Component } from 'react';

import './styles/LoaderHOC.css';

import Loader from '../components/Loader/Loader';

const LoaderHOC = (WrappedComponent) => {
    return class LoaderHOC extends Component {
        render() {
            const { colors } = this.props;
            return colors.length === 0 ? <Loader extraClass='app-load' /> : <WrappedComponent {...this.props} />
        }
    }
}

export default LoaderHOC;