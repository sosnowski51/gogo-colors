import React from 'react';
import ReactDOM from 'react-dom';
import searchColors from '../searchColors';

const data = [
    { name: 'black', id: 0, hex: '#000' },
    { name: 'white', id: 1, hex: '#fff' }
  ]

const shouldReturn = { filteredColors: [{ name: 'white', id: 1, hex: '#fff' }], showList: true };

it('should return new color', () => {
  expect(searchColors(2, 'wh', data, 2)).toEqual(shouldReturn);
});