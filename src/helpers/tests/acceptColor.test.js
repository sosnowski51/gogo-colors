import React from 'react';
import ReactDOM from 'react-dom';
import acceptColor from '../acceptColor';

const data = [
    { name: 'black', id: 0, hex: '#000' },
    { name: 'white', id: 1, hex: '#fff' }
  ]

const shouldReturn = { name: 'white', id: 1, hex: '#fff' };

it('should return new color', () => {
  expect(acceptColor('white', data)).toEqual(shouldReturn);
});