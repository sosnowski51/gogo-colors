const searchColors = (inputedChars, inputValue, colorsList, charsNumber) => {
    if (inputedChars >= charsNumber) {
        const filteredColors = colorsList.filter((color) => {
            return color.name.match(inputValue);
        })
        return filteredColors.length ? { filteredColors, showList: true } : { showList: false };
    } else {
        return { filteredColors: [], showList: false };
    }
}

export default searchColors;