const getData = (url) => {
    return fetch(url)
        .then(response => {
            if (response.ok) {
                return response.json()
                    .then(parsedResponse => parsedResponse.map((color, index) => (
                        {
                            id: index,
                            name: color.name,
                            hex: `#${color.hex}`
                        }
                    )))
                    .then(colors => { return { colors: colors }});
            } else {
                return { fetchError: true };
            }
        })
        .catch(() => { return { fetchError: true }});
}

export default getData;