const acceptColor = (inputColor, filteredColors) => {
    let choosenColor;
    if (inputColor) {
        const inputColorLowerCase = inputColor.toLowerCase();
        choosenColor = filteredColors.find(color => {
            return color.name === inputColorLowerCase;
        })
    }
    return choosenColor;
}

export default acceptColor;