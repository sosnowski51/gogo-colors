### App description ###

You can select a color and, by doing this, change the background color of the whole app. List of all colors is here http://www.mocky.io/v2/5a37a7403200000f10eb6a2d.
Colors should be selectable from an autosuggest field.

## Acceptance criteria: ##

- colors should be fetched from remote source,
- new background color must be 50% transparent,
- autosuggest should work from 2 chars.
- selected color should be accepted by clicking an �Accept� button placed next to the autosuggest field.

## Tech requirements: ##

- general stack is: React, redux, ES6
- autosuggest mechanism should be written from scratch, no ready-to-use solutions,
- code should be tested (write all suitable and needed tests),
- UI is not important,
- browser support: newest versions (skip the IE/Edge),

## To run ##

1. yarn / npm install
2. yarn start / npm run start

## To Test ##

1. First runt app to load all css files
2. yarn test / npm run test